﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="CCSymbols" Type="Str">Debug,False;lvlibp,True;</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Config" Type="Folder">
			<Item Name="System Config.ini" Type="Document" URL="../../../Exe/Luxshare-OET/Config/System Config.ini"/>
			<Item Name="QSFP28.ini" Type="Document" URL="../../../Exe/Luxshare-OET/Config/QSFP28.ini"/>
		</Item>
		<Item Name="Libraries" Type="Folder">
			<Item Name="Memory Map" Type="Folder">
				<Item Name="Control Map-QSFP28.lvlibp" Type="LVLibp" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp">
					<Item Name="Class" Type="Folder">
						<Item Name="Lower Page" Type="Folder">
							<Item Name="Monitors" Type="Folder">
								<Item Name="Revision Compliance" Type="Folder">
									<Item Name="Revision Compliance.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Revision Compliance/Revision Compliance.lvclass"/>
								</Item>
								<Item Name="STATUS (LSB)" Type="Folder">
									<Item Name="STATUS (LSB).lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/STATUS (LSB)/STATUS (LSB).lvclass"/>
								</Item>
								<Item Name="Temperature" Type="Folder">
									<Item Name="Temperature.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Temperature/Temperature.lvclass"/>
								</Item>
								<Item Name="Voltage" Type="Folder">
									<Item Name="Voltage.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Voltage/Voltage.lvclass"/>
								</Item>
								<Item Name="Propagation Delay" Type="Folder">
									<Item Name="Propagation Delay.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Propagation Delay/Propagation Delay.lvclass"/>
								</Item>
								<Item Name="Rx Power" Type="Folder">
									<Item Name="Rx Power.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Rx Power/Rx Power.lvclass"/>
								</Item>
								<Item Name="Tx Power" Type="Folder">
									<Item Name="Tx Power.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Tx Power/Tx Power.lvclass"/>
								</Item>
								<Item Name="Tx Bias" Type="Folder">
									<Item Name="Tx Bias.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Tx Bias/Tx Bias.lvclass"/>
								</Item>
								<Item Name="Lower Identifier" Type="Folder">
									<Item Name="Lower Identifier.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Lower Identifier/Lower Identifier.lvclass"/>
								</Item>
							</Item>
							<Item Name="Interrupt Flags" Type="Folder">
								<Item Name="Latched LOS" Type="Folder">
									<Item Name="Latched LOS.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Latched LOS/Latched LOS.lvclass"/>
								</Item>
								<Item Name="Latched LOL" Type="Folder">
									<Item Name="Latched LOL.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Latched LOL/Latched LOL.lvclass"/>
								</Item>
								<Item Name="Latched TX Fault" Type="Folder">
									<Item Name="Latched TX Fault.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Latched TX Fault/Latched TX Fault.lvclass"/>
								</Item>
								<Item Name="Initialization Complete Flag" Type="Folder">
									<Item Name="Initialization Complete Flag.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Initialization Complete Flag/Initialization Complete Flag.lvclass"/>
								</Item>
								<Item Name="Latched Temp_Status" Type="Folder">
									<Item Name="Latched Temp_Status.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Latched Temp_Status/Latched Temp_Status.lvclass"/>
								</Item>
								<Item Name="Latched VCC_Status" Type="Folder">
									<Item Name="Latched VCC_Status.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Latched VCC_Status/Latched VCC_Status.lvclass"/>
								</Item>
								<Item Name="Latched RX Power" Type="Folder">
									<Item Name="Latched RX Power.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Latched RX Power/Latched RX Power.lvclass"/>
								</Item>
								<Item Name="Latched TX Bias" Type="Folder">
									<Item Name="Latched TX Bias.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Latched TX Bias/Latched TX Bias.lvclass"/>
								</Item>
								<Item Name="Latched TX Power" Type="Folder">
									<Item Name="Latched TX Power.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Latched TX Power/Latched TX Power.lvclass"/>
								</Item>
							</Item>
							<Item Name="Control Function" Type="Folder">
								<Item Name="Tx Disable" Type="Folder">
									<Item Name="Tx Disable.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Tx Disable/Tx Disable.lvclass"/>
								</Item>
								<Item Name="Rx Rate Select" Type="Folder">
									<Item Name="Rx Rate Select.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Rx Rate Select/Rx Rate Select.lvclass"/>
								</Item>
								<Item Name="Tx Rate Select" Type="Folder">
									<Item Name="Tx Rate Select.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Tx Rate Select/Tx Rate Select.lvclass"/>
								</Item>
								<Item Name="Rx Application Select" Type="Folder">
									<Item Name="Rx Application Select.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Rx Application Select/Rx Application Select.lvclass"/>
								</Item>
								<Item Name="Tx Application Select" Type="Folder">
									<Item Name="Tx Application Select.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Tx Application Select/Tx Application Select.lvclass"/>
								</Item>
								<Item Name="POWER CONTROL" Type="Folder">
									<Item Name="POWER CONTROL.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/POWER CONTROL/POWER CONTROL.lvclass"/>
								</Item>
								<Item Name="CDR_CONTROL" Type="Folder">
									<Item Name="CDR_CONTROL.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/CDR_CONTROL/CDR_CONTROL.lvclass"/>
								</Item>
							</Item>
							<Item Name="Module Channel Masks" Type="Folder">
								<Item Name="Mask LOS" Type="Folder">
									<Item Name="Mask LOS.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Mask LOS/Mask LOS.lvclass"/>
								</Item>
								<Item Name="Mask Tx Adapt EQ Fault" Type="Folder">
									<Item Name="Mask Tx Adapt EQ Fault.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Mask Tx Adapt EQ Fault/Mask Tx Adapt EQ Fault.lvclass"/>
								</Item>
								<Item Name="Mask CDR LOL" Type="Folder">
									<Item Name="Mask CDR LOL.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Mask CDR LOL/Mask CDR LOL.lvclass"/>
								</Item>
								<Item Name="Mask Temp" Type="Folder">
									<Item Name="Mask Temp.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Mask Temp/Mask Temp.lvclass"/>
								</Item>
								<Item Name="Mask VCC" Type="Folder">
									<Item Name="Mask VCC.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Mask VCC/Mask VCC.lvclass"/>
								</Item>
							</Item>
						</Item>
						<Item Name="Upper Page" Type="Folder">
							<Item Name="Identifier SPEC Codes" Type="Folder">
								<Item Name="Upper Identifier" Type="Folder">
									<Item Name="Upper Identifier.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Upper Identifier/Upper Identifier.lvclass"/>
								</Item>
								<Item Name="Ext.Identifier" Type="Folder">
									<Item Name="Ext.Identifier.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Ext.Identifier/Ext.Identifier.lvclass"/>
								</Item>
								<Item Name="Connector Types" Type="Folder">
									<Item Name="Connector Types.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Connector Types/Connector Types.lvclass"/>
								</Item>
								<Item Name="Encoding" Type="Folder">
									<Item Name="Encoding.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Encoding/Encoding.lvclass"/>
								</Item>
								<Item Name="10_40G_100G Ethernet Compliance Codes" Type="Folder">
									<Item Name="10_40G_100G Ethernet Compliance Codes.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/10_40G_100G Ethernet Compliance Codes/10_40G_100G Ethernet Compliance Codes.lvclass"/>
								</Item>
								<Item Name="SONET Compliance Codes" Type="Folder">
									<Item Name="SONET Compliance Codes.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/SONET Compliance Codes/SONET Compliance Codes.lvclass"/>
								</Item>
								<Item Name="SAS_SATA Compliance Codes" Type="Folder">
									<Item Name="SAS_SATA Compliance Codes.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/SAS_SATA Compliance Codes/SAS_SATA Compliance Codes.lvclass"/>
								</Item>
								<Item Name="Gigabit Ethernet Compliant Codes" Type="Folder">
									<Item Name="Gigabit Ethernet Compliant Codes.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Gigabit Ethernet Compliant Codes/Gigabit Ethernet Compliant Codes.lvclass"/>
								</Item>
								<Item Name="Fibre Channel Link Length" Type="Folder">
									<Item Name="Fibre Channel Link Length.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Fibre Channel Link Length/Fibre Channel Link Length.lvclass"/>
								</Item>
								<Item Name="Fibre Channel Transmitter Technology" Type="Folder">
									<Item Name="Fibre Channel Transmitter Technology.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Fibre Channel Transmitter Technology/Fibre Channel Transmitter Technology.lvclass"/>
								</Item>
								<Item Name="Fibre Channel Transmission Media" Type="Folder">
									<Item Name="Fibre Channel Transmission Media.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Fibre Channel Transmission Media/Fibre Channel Transmission Media.lvclass"/>
								</Item>
								<Item Name="Fibre Channel Speed" Type="Folder">
									<Item Name="Fibre Channel Speed.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Fibre Channel Speed/Fibre Channel Speed.lvclass"/>
								</Item>
								<Item Name="Extended Rateselect Compliance" Type="Folder">
									<Item Name="Extended Rateselect Compliance.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Extended Rateselect Compliance/Extended Rateselect Compliance.lvclass"/>
								</Item>
								<Item Name="Link Codes" Type="Folder">
									<Item Name="Link Codes.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Link Codes/Link Codes.lvclass"/>
								</Item>
								<Item Name="EXTENDED MODULE" Type="Folder">
									<Item Name="EXTENDED MODULE.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/EXTENDED MODULE/EXTENDED MODULE.lvclass"/>
								</Item>
							</Item>
							<Item Name="Info" Type="Folder">
								<Item Name="BR Nominal" Type="Folder">
									<Item Name="BR Nominal.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/BR Nominal/BR Nominal.lvclass"/>
								</Item>
								<Item Name="Extended Bit Rate" Type="Folder">
									<Item Name="Extended Bit Rate.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Extended Bit Rate/Extended Bit Rate.lvclass"/>
								</Item>
								<Item Name="Length (SMF)" Type="Folder">
									<Item Name="Length (SMF).lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Length (SMF)/Length (SMF).lvclass"/>
								</Item>
								<Item Name="Length (OM3 50 um)" Type="Folder">
									<Item Name="Length (OM3 50 um).lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Length (OM3 50 um)/Length (OM3 50 um).lvclass"/>
								</Item>
								<Item Name="Length (OM2 50 um)" Type="Folder">
									<Item Name="Length (OM2 50 um).lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Length (OM2 50 um)/Length (OM2 50 um).lvclass"/>
								</Item>
								<Item Name="Length (OM1 62.5 um)" Type="Folder">
									<Item Name="Length (OM1 62.5 um).lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Length (OM1 62.5 um)/Length (OM1 62.5 um).lvclass"/>
								</Item>
								<Item Name="Length (Copper)" Type="Folder">
									<Item Name="Length (Copper).lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Length (Copper)/Length (Copper).lvclass"/>
								</Item>
								<Item Name="Device Tech" Type="Folder">
									<Item Name="Device Tech.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Device Tech/Device Tech.lvclass"/>
								</Item>
								<Item Name="Vendor Name" Type="Folder">
									<Item Name="Vendor Name.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Vendor Name/Vendor Name.lvclass"/>
								</Item>
								<Item Name="Vendor PN" Type="Folder">
									<Item Name="Vendor PN.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Vendor PN/Vendor PN.lvclass"/>
								</Item>
								<Item Name="Vendor SN" Type="Folder">
									<Item Name="Vendor SN.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Vendor SN/Vendor SN.lvclass"/>
								</Item>
								<Item Name="Vendor OUI" Type="Folder">
									<Item Name="Vendor OUI.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Vendor OUI/Vendor OUI.lvclass"/>
								</Item>
								<Item Name="Vendor Rev" Type="Folder">
									<Item Name="Vendor Rev.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Vendor Rev/Vendor Rev.lvclass"/>
								</Item>
								<Item Name="Wavelength" Type="Folder">
									<Item Name="Wavelength.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Wavelength/Wavelength.lvclass"/>
								</Item>
								<Item Name="Wavelength Tolerance" Type="Folder">
									<Item Name="Wavelength Tolerance.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Wavelength Tolerance/Wavelength Tolerance.lvclass"/>
								</Item>
								<Item Name="Max Case Temp" Type="Folder">
									<Item Name="Max Case Temp.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Max Case Temp/Max Case Temp.lvclass"/>
								</Item>
								<Item Name="CC_BASE" Type="Folder">
									<Item Name="CC_BASE.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/CC_BASE/CC_BASE.lvclass"/>
								</Item>
								<Item Name="DATE YEAR CODE" Type="Folder">
									<Item Name="DATE YEAR CODE.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/DATE YEAR CODE/DATE YEAR CODE.lvclass"/>
								</Item>
								<Item Name="DATE MONTH CODE" Type="Folder">
									<Item Name="DATE MONTH CODE.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/DATE MONTH CODE/DATE MONTH CODE.lvclass"/>
								</Item>
								<Item Name="DATE DAY CODE" Type="Folder">
									<Item Name="DATE DAY CODE.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/DATE DAY CODE/DATE DAY CODE.lvclass"/>
								</Item>
								<Item Name="Vendor Specific lot code" Type="Folder">
									<Item Name="Vendor Specific lot code.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Vendor Specific lot code/Vendor Specific lot code.lvclass"/>
								</Item>
								<Item Name="DIAGNOSTIC MONITORING TYPE" Type="Folder">
									<Item Name="DIAGNOSTIC MONITORING TYPE.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/DIAGNOSTIC MONITORING TYPE/DIAGNOSTIC MONITORING TYPE.lvclass"/>
								</Item>
								<Item Name="ENHANCED OPTIONS" Type="Folder">
									<Item Name="ENHANCED OPTIONS.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/ENHANCED OPTIONS/ENHANCED OPTIONS.lvclass"/>
								</Item>
								<Item Name="CC_EXT" Type="Folder">
									<Item Name="CC_EXT.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/CC_EXT/CC_EXT.lvclass"/>
								</Item>
							</Item>
							<Item Name="Option Values" Type="Folder">
								<Item Name="Tx Input EQ and Rx Output Emphasis" Type="Folder">
									<Item Name="Tx Input EQ and Rx Output Emphasis.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Tx Input EQ and Rx Output Emphasis/Tx Input EQ and Rx Output Emphasis.lvclass"/>
								</Item>
								<Item Name="CDR Status and Control Functions" Type="Folder">
									<Item Name="CDR Status and Control Functions.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/CDR Status and Control Functions/CDR Status and Control Functions.lvclass"/>
								</Item>
								<Item Name="Options" Type="Folder">
									<Item Name="Options.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Options/Options.lvclass"/>
								</Item>
							</Item>
						</Item>
						<Item Name="Upper Page3" Type="Folder">
							<Item Name="Thresholds" Type="Folder">
								<Item Name="Temp Threshold" Type="Folder">
									<Item Name="Temp Threshold.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Temp Threshold/Temp Threshold.lvclass"/>
								</Item>
								<Item Name="VCC Threshold" Type="Folder">
									<Item Name="VCC Threshold.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/VCC Threshold/VCC Threshold.lvclass"/>
								</Item>
								<Item Name="Rx Power Threshold" Type="Folder">
									<Item Name="Rx Power Threshold.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Rx Power Threshold/Rx Power Threshold.lvclass"/>
								</Item>
								<Item Name="Tx Bias Threshold" Type="Folder">
									<Item Name="Tx Bias Threshold.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Tx Bias Threshold/Tx Bias Threshold.lvclass"/>
								</Item>
								<Item Name="Tx Power Threshold" Type="Folder">
									<Item Name="Tx Power Threshold.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Tx Power Threshold/Tx Power Threshold.lvclass"/>
								</Item>
							</Item>
							<Item Name="Optional Controls" Type="Folder">
								<Item Name="TX EQ_RX EMPHASIS MAGNITUDE ID" Type="Folder">
									<Item Name="TX EQ_RX EMPHASIS MAGNITUDE ID.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/TX EQ_RX EMPHASIS MAGNITUDE ID/TX EQ_RX EMPHASIS MAGNITUDE ID.lvclass"/>
								</Item>
								<Item Name="RX Output Amplitude Support" Type="Folder">
									<Item Name="RX Output Amplitude Support.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/RX Output Amplitude Support/RX Output Amplitude Support.lvclass"/>
								</Item>
								<Item Name="TX Input Equalization" Type="Folder">
									<Item Name="TX Input Equalization.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/TX Input Equalization/TX Input Equalization.lvclass"/>
								</Item>
								<Item Name="RX Output Emphasis" Type="Folder">
									<Item Name="RX Output Emphasis.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/RX Output Emphasis/RX Output Emphasis.lvclass"/>
								</Item>
								<Item Name="Output Amplitude No Output Equalization" Type="Folder">
									<Item Name="Output Amplitude No Output Equalization.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Output Amplitude No Output Equalization/Output Amplitude No Output Equalization.lvclass"/>
								</Item>
								<Item Name="RX SQ" Type="Folder">
									<Item Name="RX SQ.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/RX SQ/RX SQ.lvclass"/>
								</Item>
								<Item Name="RX OUTPUT" Type="Folder">
									<Item Name="RX OUTPUT.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/RX OUTPUT/RX OUTPUT.lvclass"/>
								</Item>
							</Item>
							<Item Name="Channel Monitor Masks" Type="Folder">
								<Item Name="Mask RX Power" Type="Folder">
									<Item Name="Mask RX Power.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Mask RX Power/Mask RX Power.lvclass"/>
								</Item>
								<Item Name="Mask Tx Bias" Type="Folder">
									<Item Name="Mask Tx Bias.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Mask Tx Bias/Mask Tx Bias.lvclass"/>
								</Item>
								<Item Name="Mask TX Power" Type="Folder">
									<Item Name="Mask TX Power.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Mask TX Power/Mask TX Power.lvclass"/>
								</Item>
							</Item>
						</Item>
						<Item Name="Memory Map" Type="Folder">
							<Item Name="Lower Page" Type="Folder">
								<Item Name="Lower Page.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Lower Page/Lower Page.lvclass"/>
							</Item>
							<Item Name="Upper Page" Type="Folder">
								<Item Name="Upper Page.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Upper Page/Upper Page.lvclass"/>
							</Item>
							<Item Name="Upper Page3" Type="Folder">
								<Item Name="Upper Page3.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Class/Upper Page3/Upper Page3.lvclass"/>
							</Item>
						</Item>
					</Item>
					<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/1abvi3w/vi.lib/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
					<Item Name="Application Directory.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/1abvi3w/vi.lib/Utility/file.llb/Application Directory.vi"/>
					<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
					<Item Name="Control Map-QSFP28.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/Control Map-QSFP28.lvclass"/>
					<Item Name="Control Refnum.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/1abvi3w/menus/Categories/User Library/Application Control/Control Refnum/Control Refnum.lvlib"/>
					<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
					<Item Name="Get System Directory.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/Get System Directory.vi"/>
					<Item Name="Luxshare-OET API.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/1abvi3w/menus/Categories/Luxshare-OET/Luxshare-OET API.lvlib"/>
					<Item Name="LVRowAndColumnTypeDef.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVRowAndColumnTypeDef.ctl"/>
					<Item Name="NI_FileType.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/1abvi3w/vi.lib/Utility/lvfile.llb/NI_FileType.lvlib"/>
					<Item Name="NI_LVConfig.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/1abvi3w/vi.lib/Utility/config.llb/NI_LVConfig.lvlib"/>
					<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/1abvi3w/vi.lib/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
					<Item Name="Space Constant.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/1abvi3w/vi.lib/dlg_ctls.llb/Space Constant.vi"/>
					<Item Name="System Directory Type.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/System Directory Type.ctl"/>
					<Item Name="Table-Auto Scroll.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/SubVIs/Table-Auto Scroll.vi"/>
					<Item Name="Trim Whitespace.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
					<Item Name="whitespace.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Control Map-QSFP28.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
				</Item>
				<Item Name="Internal Calibration.lvlibp" Type="LVLibp" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp">
					<Item Name="Class" Type="Folder">
						<Item Name="OSFP" Type="Folder">
							<Item Name="OSFP.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/Class/OSFP/OSFP.lvclass"/>
						</Item>
						<Item Name="QSFP-DD" Type="Folder">
							<Item Name="QSFP-DD.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/Class/QSFP-DD/QSFP-DD.lvclass"/>
						</Item>
						<Item Name="QSFP28" Type="Folder">
							<Item Name="QSFP28-Internal" Type="Folder">
								<Item Name="QSFP28-Internal.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/Class/QSFP28-Internal/QSFP28-Internal.lvclass"/>
							</Item>
						</Item>
						<Item Name="SFP28" Type="Folder">
							<Item Name="Debug Bytes" Type="Folder">
								<Item Name="Debug Bytes.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/Class/Debug Bytes/Debug Bytes.lvclass"/>
							</Item>
							<Item Name="High Low Temp" Type="Folder">
								<Item Name="High Low Temp.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/Class/High Low Temp/High Low Temp.lvclass"/>
							</Item>
							<Item Name="IBias Slope Offset" Type="Folder">
								<Item Name="IBias Slope Offset.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/Class/IBias Slope Offset/IBias Slope Offset.lvclass"/>
							</Item>
							<Item Name="RX LOS Assert" Type="Folder">
								<Item Name="RX LOS Assert.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/Class/RX LOS Assert/RX LOS Assert.lvclass"/>
							</Item>
							<Item Name="RXP Slope Offset" Type="Folder">
								<Item Name="RXP Slope Offset.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/Class/RXP Slope Offset/RXP Slope Offset.lvclass"/>
							</Item>
							<Item Name="TXP Slope Offset" Type="Folder">
								<Item Name="TXP Slope Offset.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/Class/TXP Slope Offset/TXP Slope Offset.lvclass"/>
							</Item>
							<Item Name="VCC and Temp" Type="Folder">
								<Item Name="VCC and Temp.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/Class/VCC and Temp/VCC and Temp.lvclass"/>
							</Item>
						</Item>
						<Item Name="Table" Type="Folder">
							<Item Name="Page 90.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/Class/Page 90/Page 90.lvclass"/>
						</Item>
					</Item>
					<Item Name="SubVIs" Type="Folder">
						<Item Name="Table-Auto Scroll.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/SubVIs/Table-Auto Scroll.vi"/>
					</Item>
					<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/vi.lib/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
					<Item Name="Application Directory.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/vi.lib/Utility/file.llb/Application Directory.vi"/>
					<Item Name="Array of VData to VCluster__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Array of VData to VCluster__ogtk.vi"/>
					<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/user.lib/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
					<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
					<Item Name="Clear Errors.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
					<Item Name="Cluster to Array of VData__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Cluster to Array of VData__ogtk.vi"/>
					<Item Name="Control Refnum.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/menus/Categories/User Library/Application Control/Control Refnum/Control Refnum.lvlib"/>
					<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
					<Item Name="Get Cluster Element by Name__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Element by Name__ogtk.vi"/>
					<Item Name="Get Cluster Element Names__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Element Names__ogtk.vi"/>
					<Item Name="Get Cluster Elements TDs__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Elements TDs__ogtk.vi"/>
					<Item Name="Get Data Name from TD__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Data Name from TD__ogtk.vi"/>
					<Item Name="Get Data Name__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Data Name__ogtk.vi"/>
					<Item Name="Get Header from TD__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Header from TD__ogtk.vi"/>
					<Item Name="Get Last PString__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Last PString__ogtk.vi"/>
					<Item Name="Get PString__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get PString__ogtk.vi"/>
					<Item Name="Get System Directory.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/Get System Directory.vi"/>
					<Item Name="Get Variant Attributes__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Variant Attributes__ogtk.vi"/>
					<Item Name="Internal Calibration.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/Internal Calibration.lvclass"/>
					<Item Name="Luxshare-OET API.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/menus/Categories/Luxshare-OET/Luxshare-OET API.lvlib"/>
					<Item Name="LVRowAndColumnTypeDef.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVRowAndColumnTypeDef.ctl"/>
					<Item Name="MulticolumnListbox.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/menus/Categories/User Library/Application Control/MulticolumnListbox/MulticolumnListbox.lvlib"/>
					<Item Name="NI_FileType.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/vi.lib/Utility/lvfile.llb/NI_FileType.lvlib"/>
					<Item Name="NI_LVConfig.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/vi.lib/Utility/config.llb/NI_LVConfig.lvlib"/>
					<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/vi.lib/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
					<Item Name="Parse String with TDs__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Parse String with TDs__ogtk.vi"/>
					<Item Name="Set Cluster Element by Name__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Set Cluster Element by Name__ogtk.vi"/>
					<Item Name="Set Data Name__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Set Data Name__ogtk.vi"/>
					<Item Name="Split Cluster TD__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Split Cluster TD__ogtk.vi"/>
					<Item Name="System Directory Type.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/System Directory Type.ctl"/>
					<Item Name="Trim Whitespace.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
					<Item Name="Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Enumeration__ogtk.ctl"/>
					<Item Name="Type Descriptor Header__ogtk.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Header__ogtk.ctl"/>
					<Item Name="Type Descriptor__ogtk.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor__ogtk.ctl"/>
					<Item Name="Variant to Header Info__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Variant to Header Info__ogtk.vi"/>
					<Item Name="whitespace.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Internal Calibration.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
				</Item>
				<Item Name="Other Pages.lvlibp" Type="LVLibp" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp">
					<Item Name="Class" Type="Folder">
						<Item Name="Page B0" Type="Folder">
							<Item Name="Page B0.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/Class/Page B0/Page B0.lvclass"/>
						</Item>
					</Item>
					<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/1abvi3w/vi.lib/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
					<Item Name="Application Directory.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/1abvi3w/vi.lib/Utility/file.llb/Application Directory.vi"/>
					<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
					<Item Name="Clear Errors.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
					<Item Name="Control Refnum.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/1abvi3w/menus/Categories/User Library/Application Control/Control Refnum/Control Refnum.lvlib"/>
					<Item Name="Delete Elements from 1D Array (U8)__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (U8)__ogtk.vi"/>
					<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
					<Item Name="Filter 1D Array (U8)__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Filter 1D Array (U8)__ogtk.vi"/>
					<Item Name="Get System Directory.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/Get System Directory.vi"/>
					<Item Name="Luxshare-OET API.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/1abvi3w/menus/Categories/Luxshare-OET/Luxshare-OET API.lvlib"/>
					<Item Name="LVRowAndColumnTypeDef.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVRowAndColumnTypeDef.ctl"/>
					<Item Name="MGI Caller&apos;s VI Reference.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/1abvi3w/user.lib/_MGI/Application Control/MGI VI Reference/MGI Caller&apos;s VI Reference.vi"/>
					<Item Name="MGI Defer Panel Updates.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/1abvi3w/user.lib/_MGI/Application Control/MGI Defer Panel Updates.vi"/>
					<Item Name="NI_Data Type.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/1abvi3w/vi.lib/Utility/Data Type/NI_Data Type.lvlib"/>
					<Item Name="NI_FileType.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/1abvi3w/vi.lib/Utility/lvfile.llb/NI_FileType.lvlib"/>
					<Item Name="NI_LVConfig.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/1abvi3w/vi.lib/Utility/config.llb/NI_LVConfig.lvlib"/>
					<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/1abvi3w/vi.lib/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
					<Item Name="Other Pages.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/Other Pages.lvclass"/>
					<Item Name="Remove Duplicates from 1D Array (U8)__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U8)__ogtk.vi"/>
					<Item Name="Reorder 1D Array2 (U8)__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Reorder 1D Array2 (U8)__ogtk.vi"/>
					<Item Name="Search 1D Array (U8)__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Search 1D Array (U8)__ogtk.vi"/>
					<Item Name="Sort 1D Array (I32)__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Sort 1D Array (I32)__ogtk.vi"/>
					<Item Name="System Directory Type.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/System Directory Type.ctl"/>
					<Item Name="Table-Auto Scroll.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/SubVIs/Table-Auto Scroll.vi"/>
					<Item Name="Trim Whitespace.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
					<Item Name="whitespace.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Other Pages.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
				</Item>
				<Item Name="Memory Map.lvlibp" Type="LVLibp" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp">
					<Item Name="SubVIs" Type="Folder">
						<Item Name="Convert Control Value to Byte.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/SubVIs/Convert Control Value to Byte.vi"/>
						<Item Name="Get Control Child Class.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/SubVIs/Get Control Child Class.vi"/>
						<Item Name="Merge Hi Low Byte-Array.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/SubVIs/Merge Hi Low Byte-Array.vi"/>
						<Item Name="Merge Hi Low Byte-Single.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/SubVIs/Merge Hi Low Byte-Single.vi"/>
						<Item Name="Merge Hi Low Byte.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/SubVIs/Merge Hi Low Byte.vi"/>
						<Item Name="Write To File.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/SubVIs/Write To File.vi"/>
					</Item>
					<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/1abvi3w/vi.lib/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
					<Item Name="Application Directory.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/1abvi3w/vi.lib/Utility/file.llb/Application Directory.vi"/>
					<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
					<Item Name="Clear Errors.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
					<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
					<Item Name="Get LV Class Default Value.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Get LV Class Default Value.vi"/>
					<Item Name="Get LV Class Name.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Get LV Class Name.vi"/>
					<Item Name="Get LV Class Path.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Get LV Class Path.vi"/>
					<Item Name="Get System Directory.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/Get System Directory.vi"/>
					<Item Name="List Directory and LLBs.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/List Directory and LLBs.vi"/>
					<Item Name="Luxshare-OET API.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/1abvi3w/menus/Categories/Luxshare-OET/Luxshare-OET API.lvlib"/>
					<Item Name="Memory Map.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/Class/Memory Map.lvclass"/>
					<Item Name="NI_FileType.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/1abvi3w/vi.lib/Utility/lvfile.llb/NI_FileType.lvlib"/>
					<Item Name="NI_LVConfig.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/1abvi3w/vi.lib/Utility/config.llb/NI_LVConfig.lvlib"/>
					<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/1abvi3w/vi.lib/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
					<Item Name="Recursive File List.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Recursive File List.vi"/>
					<Item Name="System Directory Type.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/System Directory Type.ctl"/>
					<Item Name="Trim Whitespace.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
					<Item Name="whitespace.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
					<Item Name="Write Delimited Spreadsheet (string).vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/1abvi3w/vi.lib/Utility/file.llb/Write Delimited Spreadsheet (string).vi"/>
					<Item Name="Write Spreadsheet String.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Memory Map/Memory Map.lvlibp/1abvi3w/vi.lib/Utility/file.llb/Write Spreadsheet String.vi"/>
				</Item>
			</Item>
			<Item Name="Plugins" Type="Folder">
				<Item Name="UI Reference.lvlibp" Type="LVLibp" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp">
					<Item Name="Controls" Type="Folder">
						<Item Name="UI - All UI Objects Refs Memory - Operation.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/Controls/UI - All UI Objects Refs Memory - Operation.ctl"/>
					</Item>
					<Item Name="SubVIs" Type="Folder">
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Array.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Array.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Boolean.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Boolean.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Cluster.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Cluster.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - ColorBox.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - ColorBox.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - ColorRamp.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - ColorRamp.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - ComboBox.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - ComboBox.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - DAQmxName.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - DAQmxName.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - DataValRefNum.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - DataValRefNum.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Digital.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Digital.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - DigitalGraph.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - DigitalGraph.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Enum.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Enum.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - GraphChart.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - GraphChart.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - IntensityChart.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - IntensityChart.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - IntensityGraph.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - IntensityGraph.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Knob.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Knob.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - ListBox.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - ListBox.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - LVObjectRefNum.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - LVObjectRefNum.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - MixedCheckbox.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - MixedCheckbox.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - MixedSignalGraph.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - MixedSignalGraph.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - MulticolumnListbox.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - MulticolumnListbox.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - MultiSegmentPipe.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - MultiSegmentPipe.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - NamedNumeric.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - NamedNumeric.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Numeric.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Numeric.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - NumericWithScale.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - NumericWithScale.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - PageSelector.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - PageSelector.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Path.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Path.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Picture.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Picture.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Pixmap.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Pixmap.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - RadioButtonsControl.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - RadioButtonsControl.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - RefNum.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - RefNum.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Ring.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Ring.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - SceneGraphDisplay.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - SceneGraphDisplay.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Scrollbar.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Scrollbar.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Slide.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Slide.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - String.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - String.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - SubPanel.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - SubPanel.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - TabControl.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - TabControl.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - Table.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Table.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - TreeControl.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - TreeControl.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - TypedRefNum.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - TypedRefNum.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - VIRefNum.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - VIRefNum.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - WaveformChart.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - WaveformChart.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - WaveformData.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - WaveformData.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - WaveformGraph.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - WaveformGraph.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Get Reference - XYGraph.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - XYGraph.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - String version.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - String version.vi"/>
						<Item Name="UI - All UI Objects Refs Memory - Typedef version.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Typedef version.vi"/>
					</Item>
					<Item Name="Init Buttons.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/Init Buttons.vi"/>
					<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/UI - All UI Objects Refs Memory - Get Reference.vi"/>
					<Item Name="UI - All UI Objects Refs Memory.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/UI Reference.lvlibp/SubVIs/UI - All UI Objects Refs Memory.vi"/>
				</Item>
				<Item Name="Optical Product.lvlibp" Type="LVLibp" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp">
					<Item Name="Palette" Type="Folder">
						<Item Name="Include mnu" Type="Folder">
							<Item Name="Product Calibration Page Assert &amp; Deassert.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Calibration Page Assert &amp; Deassert.mnu"/>
							<Item Name="Product Calibration Page Offset.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Calibration Page Offset.mnu"/>
							<Item Name="Product Calibration Page Slope.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Calibration Page Slope.mnu"/>
							<Item Name="Product Calibration Page Function.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Calibration Page Function.mnu"/>
							<Item Name="Product Calibration Page 2 Point.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Calibration Page 2 Point.mnu"/>
							<Item Name="Product Calibration Page.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Calibration Page.mnu"/>
							<Item Name="Product Communication.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Communication.mnu"/>
							<Item Name="Product ID Info Page.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product ID Info Page.mnu"/>
							<Item Name="Product Information.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Information.mnu"/>
							<Item Name="Product Information QSFP-DD.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Information QSFP-DD.mnu"/>
							<Item Name="Product Interrupt Flag.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Interrupt Flag.mnu"/>
							<Item Name="Product Monitor.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Monitor.mnu"/>
							<Item Name="Product Page Function.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Page Function.mnu"/>
							<Item Name="Product Threshold.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Threshold.mnu"/>
							<Item Name="Product Class.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Class.mnu"/>
							<Item Name="Product Control Function QSFP.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Control Function QSFP.mnu"/>
							<Item Name="Product Control Function QSFP-DD.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Control Function QSFP-DD.mnu"/>
							<Item Name="Product Control Function SFP.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Control Function SFP.mnu"/>
							<Item Name="Product Control Function.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Control Function.mnu"/>
							<Item Name="Product Interrupt Flag QSFP-DD.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Interrupt Flag QSFP-DD.mnu"/>
							<Item Name="Product Interrupt Flag QSFP.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Interrupt Flag QSFP.mnu"/>
							<Item Name="Product Interrupt Flag SFP.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Interrupt Flag SFP.mnu"/>
							<Item Name="Product MSA Optional Page.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product MSA Optional Page.mnu"/>
							<Item Name="Product Lookup Table Page.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Lookup Table Page.mnu"/>
							<Item Name="Product Control Function PPG&amp;Loopback Control  QSFP-DD.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Control Function PPG&amp;Loopback Control  QSFP-DD.mnu"/>
						</Item>
						<Item Name="Product.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product.mnu"/>
					</Item>
					<Item Name="Optical Product" Type="Folder">
						<Item Name="Optical Product.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Optical Product.lvclass"/>
					</Item>
					<Item Name="MSA" Type="Folder">
						<Item Name="QSFP-DD" Type="Folder">
							<Item Name="QSFP-DD.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/QSFP-DD/QSFP-DD.lvclass"/>
						</Item>
						<Item Name="QSFP56 CMIS" Type="Folder">
							<Item Name="QSFP56 CMIS.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/QSFP56 CMIS/QSFP56 CMIS.lvclass"/>
						</Item>
						<Item Name="QSFP56 SFF8636" Type="Folder">
							<Item Name="QSFP56 SFF8636.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/QSFP56 SFF8636/QSFP56 SFF8636.lvclass"/>
						</Item>
						<Item Name="QSFP28" Type="Folder">
							<Item Name="QSFP28.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/QSFP28/QSFP28.lvclass"/>
						</Item>
						<Item Name="QSFP10" Type="Folder">
							<Item Name="QSFP10.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/QSFP10/QSFP10.lvclass"/>
						</Item>
						<Item Name="SFP-DD" Type="Folder">
							<Item Name="SFP-DD.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/SFP-DD/SFP-DD.lvclass"/>
						</Item>
						<Item Name="SFP28" Type="Folder">
							<Item Name="SFP28.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/SFP28/SFP28.lvclass"/>
						</Item>
						<Item Name="SFP28-EFM8BB1" Type="Folder">
							<Item Name="SFP28-EFM8BB1.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/SFP28-EFM8BB1/SFP28-EFM8BB1.lvclass"/>
						</Item>
						<Item Name="SFP10" Type="Folder">
							<Item Name="SFP10.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/SFP10/SFP10.lvclass"/>
						</Item>
						<Item Name="OSFP" Type="Folder">
							<Item Name="OSFP.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/OSFP/OSFP.lvclass"/>
						</Item>
						<Item Name="COBO" Type="Folder">
							<Item Name="COBO.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/COBO/COBO.lvclass"/>
						</Item>
						<Item Name="QM Products" Type="Folder">
							<Item Name="QM SFP28" Type="Folder">
								<Item Name="QM SFP28.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/QM SFP28/QM SFP28.lvclass"/>
							</Item>
						</Item>
					</Item>
					<Item Name="Tx Device" Type="Folder">
						<Item Name="Tx Device.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Tx Device/Tx Device.lvclass"/>
					</Item>
					<Item Name="Rx Device" Type="Folder">
						<Item Name="Rx Device.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Rx Device/Rx Device.lvclass"/>
					</Item>
					<Item Name="PSM4 &amp; CWMD4 Device" Type="Folder">
						<Item Name="24025" Type="Folder">
							<Item Name="24025.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/24025/24025.lvclass"/>
						</Item>
					</Item>
					<Item Name="AOC TRx Device" Type="Folder">
						<Item Name="37045" Type="Folder">
							<Item Name="37045.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/37045/37045.lvclass"/>
						</Item>
						<Item Name="37145" Type="Folder">
							<Item Name="37145.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/37145/37145.lvclass"/>
						</Item>
						<Item Name="37345" Type="Folder">
							<Item Name="37345.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/37345/37345.lvclass"/>
						</Item>
						<Item Name="37645" Type="Folder">
							<Item Name="37645.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/37645/37645.lvclass"/>
						</Item>
						<Item Name="37044" Type="Folder">
							<Item Name="37044.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/37044/37044.lvclass"/>
						</Item>
						<Item Name="37144" Type="Folder">
							<Item Name="37144.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/37144/37144.lvclass"/>
						</Item>
						<Item Name="37344" Type="Folder">
							<Item Name="37344.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/37344/37344.lvclass"/>
						</Item>
						<Item Name="37644" Type="Folder">
							<Item Name="37644.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/37644/37644.lvclass"/>
						</Item>
						<Item Name="RT146" Type="Folder">
							<Item Name="RT146.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/RT146/RT146.lvclass"/>
						</Item>
						<Item Name="RT145" Type="Folder">
							<Item Name="RT145.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/RT145/RT145.lvclass"/>
						</Item>
						<Item Name="UX2291" Type="Folder">
							<Item Name="UX2291.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/UX2291/UX2291.lvclass"/>
						</Item>
						<Item Name="UX2091" Type="Folder">
							<Item Name="UX2091.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/UX2091/UX2091.lvclass"/>
						</Item>
					</Item>
					<Item Name="Public" Type="Folder">
						<Item Name="Scan Product.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Public/Scan Product.vi"/>
						<Item Name="Scan Product By String.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Public/Scan Product By String.vi"/>
						<Item Name="Scan Product By Identifier.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Public/Scan Product By Identifier.vi"/>
						<Item Name="Replace USB-I2C Class.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Public/Replace USB-I2C Class.vi"/>
						<Item Name="Get Product Index.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Public/Get Product Index.vi"/>
						<Item Name="Get Product Class.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Public/Get Product Class.vi"/>
						<Item Name="Get Product Channel.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Public/Get Product Channel.vi"/>
						<Item Name="Get Slave Address.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Public/Get Slave Address.vi"/>
						<Item Name="Replace Slave Address.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Public/Replace Slave Address.vi"/>
						<Item Name="Scan Outsourcing Product.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Public/Scan Outsourcing Product.vi"/>
						<Item Name="Get Outsourcing Product Class.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Public/Get Outsourcing Product Class.vi"/>
					</Item>
					<Item Name="Luxshare-OET API.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/menus/Categories/Luxshare-OET/Luxshare-OET API.lvlib"/>
					<Item Name="NI_FileType.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/lvfile.llb/NI_FileType.lvlib"/>
					<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
					<Item Name="NI_LVConfig.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/config.llb/NI_LVConfig.lvlib"/>
					<Item Name="Lookup Table.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/menus/Categories/Luxshare-OET/Lookup Table/Lookup Table.lvlib"/>
					<Item Name="GPString.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/GPower/String/GPString.lvlib"/>
					<Item Name="GPNumeric.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/GPower/Numeric/GPNumeric.lvlib"/>
					<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
					<Item Name="subTimeDelay.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
					<Item Name="Stall Data Flow.vim" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/Stall Data Flow.vim"/>
					<Item Name="Number To Enum.vim" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/Number To Enum.vim"/>
					<Item Name="Space Constant.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/dlg_ctls.llb/Space Constant.vi"/>
					<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
					<Item Name="Type Descriptor__ogtk.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor__ogtk.ctl"/>
					<Item Name="Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Enumeration__ogtk.ctl"/>
					<Item Name="Type Descriptor Header__ogtk.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Header__ogtk.ctl"/>
					<Item Name="Get Header from TD__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Header from TD__ogtk.vi"/>
					<Item Name="Split Cluster TD__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Split Cluster TD__ogtk.vi"/>
					<Item Name="Get Cluster Elements TDs__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Elements TDs__ogtk.vi"/>
					<Item Name="Get PString__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get PString__ogtk.vi"/>
					<Item Name="Get Last PString__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Last PString__ogtk.vi"/>
					<Item Name="Get Data Name from TD__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Data Name from TD__ogtk.vi"/>
					<Item Name="Get Cluster Element Names__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Element Names__ogtk.vi"/>
					<Item Name="LVOOP Get Default Object__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/LVOOP Get Default Object__ogtk.vi"/>
					<Item Name="Qualified Name Array To Single String.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Qualified Name Array To Single String.vi"/>
					<Item Name="LVOOP Return Class Name__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/LVOOP Return Class Name__ogtk.vi"/>
					<Item Name="System Directory Type.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/System Directory Type.ctl"/>
					<Item Name="Get System Directory.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/Get System Directory.vi"/>
					<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
					<Item Name="whitespace.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
					<Item Name="Trim Whitespace.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
					<Item Name="Clear Errors.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
					<Item Name="Application Directory.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/file.llb/Application Directory.vi"/>
					<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
					<Item Name="LVOOP Is Same Or Descendant Class__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/LVOOP Is Same Or Descendant Class__ogtk.vi"/>
					<Item Name="Get LV Class Name.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Get LV Class Name.vi"/>
					<Item Name="Channel DBL-Cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Control/Channel DBL-Cluster.ctl"/>
					<Item Name="Channel U16-Cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Control/Channel U16-Cluster.ctl"/>
					<Item Name="Function-Enum.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Control/Function-Enum.ctl"/>
				</Item>
				<Item Name="DVR.lvlibp" Type="LVLibp" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp">
					<Item Name="Type Definitions" Type="Folder">
						<Item Name="Queue Loop Data Type.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/Type Definitions/Queue Loop Data Type.ctl"/>
						<Item Name="Data Queue.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/Type Definitions/Data Queue.ctl"/>
					</Item>
					<Item Name="Methods" Type="Folder">
						<Item Name="Send Command To Queue.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/Method/Send Command To Queue.vi"/>
						<Item Name="Obtain Data Queue.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/Method/Obtain Data Queue.vi"/>
						<Item Name="Send Data To Data Queue.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/Method/Send Data To Data Queue.vi"/>
						<Item Name="Get Data From Data Queue.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/Method/Get Data From Data Queue.vi"/>
						<Item Name="Flush Data Queue.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/Method/Flush Data Queue.vi"/>
					</Item>
					<Item Name="APIs" Type="Folder">
						<Item Name="Function" Type="Folder">
							<Item Name="Create.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Create.vi"/>
							<Item Name="Destory.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Destory.vi"/>
							<Item Name="Delete.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Delete.vi"/>
						</Item>
						<Item Name="Boolean" Type="Folder">
							<Item Name="Get Boolean.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get Boolean.vi"/>
							<Item Name="Get 1D Boolean.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get 1D Boolean.vi"/>
							<Item Name="Get 2D Boolean.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get 2D Boolean.vi"/>
						</Item>
						<Item Name="DBL" Type="Folder">
							<Item Name="Get DBL.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get DBL.vi"/>
							<Item Name="Get 1D DBL.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get 1D DBL.vi"/>
							<Item Name="Get 2D DBL.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get 2D DBL.vi"/>
						</Item>
						<Item Name="String" Type="Folder">
							<Item Name="Get String.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get String.vi"/>
							<Item Name="Get 1D String.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get 1D String.vi"/>
							<Item Name="Get 2D String.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get 2D String.vi"/>
						</Item>
						<Item Name="U8" Type="Folder">
							<Item Name="Get U8.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get U8.vi"/>
							<Item Name="Get 1D U8.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get 1D U8.vi"/>
							<Item Name="Get 2D U8.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get 2D U8.vi"/>
						</Item>
						<Item Name="U16" Type="Folder">
							<Item Name="Get U16.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get U16.vi"/>
							<Item Name="Get 1D U16.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get 1D U16.vi"/>
							<Item Name="Get 2D U16.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get 2D U16.vi"/>
						</Item>
						<Item Name="U32" Type="Folder">
							<Item Name="Get U32.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get U32.vi"/>
							<Item Name="Get 1D U32.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get 1D U32.vi"/>
							<Item Name="Get 2D U32.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get 2D U32.vi"/>
						</Item>
						<Item Name="I32" Type="Folder">
							<Item Name="Get I32.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get I32.vi"/>
							<Item Name="Get 1D I32.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get 1D I32.vi"/>
							<Item Name="Get 2D I32.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get 2D I32.vi"/>
						</Item>
						<Item Name="U64" Type="Folder">
							<Item Name="Get U64.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get U64.vi"/>
							<Item Name="Get 1D U64.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get 1D U64.vi"/>
							<Item Name="Get 2D U64.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get 2D U64.vi"/>
						</Item>
						<Item Name="Path" Type="Folder">
							<Item Name="Get Path.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get Path.vi"/>
							<Item Name="Get 1D Path.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get 1D Path.vi"/>
						</Item>
						<Item Name="Cluster" Type="Folder">
							<Item Name="Get Cluster.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get Cluster.vi"/>
						</Item>
						<Item Name="Class" Type="Folder">
							<Item Name="Get USB-I2C Class.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get USB-I2C Class.vi"/>
						</Item>
						<Item Name="Refnum" Type="Folder">
							<Item Name="Get Config Refnum.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get Config Refnum.vi"/>
						</Item>
						<Item Name="Variant" Type="Folder">
							<Item Name="Get Variant.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Get Variant.vi"/>
						</Item>
						<Item Name="Set Data to Variant.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/APIs/Set Data to Variant.vi"/>
					</Item>
					<Item Name="Private SubVIs" Type="Folder">
						<Item Name="Get Data Type.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/Private SubVIs/Get Data Type.vi"/>
					</Item>
					<Item Name="Private Controls" Type="Folder">
						<Item Name="DVR Data Type.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/Private Controls/DVR Data Type.ctl"/>
						<Item Name="DVR Data Value Reference.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/Private Controls/DVR Data Value Reference.ctl"/>
					</Item>
					<Item Name="Public Control" Type="Folder">
						<Item Name="Type-Enum.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/Public Controls/Type-Enum.ctl"/>
					</Item>
					<Item Name="DVR Poly.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/DVR Poly.vi"/>
					<Item Name="NI_Data Type.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/1abvi3w/vi.lib/Utility/Data Type/NI_Data Type.lvlib"/>
					<Item Name="NI_LVConfig.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/1abvi3w/vi.lib/Utility/config.llb/NI_LVConfig.lvlib"/>
					<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/Plugins/DVR.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
				</Item>
			</Item>
			<Item Name="USB Communication" Type="Folder">
				<Item Name="USB-I2C" Type="Folder">
					<Item Name="USB-I2C.lvlibp" Type="LVLibp" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp">
						<Item Name="Palette" Type="Folder">
							<Item Name="USB-I2C.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/USB-I2C.mnu"/>
							<Item Name="USB-I2C Example.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/USB-I2C Example.mnu"/>
						</Item>
						<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
						<Item Name="Clear Errors.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
						<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
						<Item Name="Get Cluster Element Names__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Element Names__ogtk.vi"/>
						<Item Name="Get Cluster Elements TDs__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Elements TDs__ogtk.vi"/>
						<Item Name="Get Data Name from TD__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Data Name from TD__ogtk.vi"/>
						<Item Name="Get File Extension.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Get File Extension.vi"/>
						<Item Name="Get Header from TD__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Header from TD__ogtk.vi"/>
						<Item Name="Get Last PString__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Last PString__ogtk.vi"/>
						<Item Name="Get LV Class Default Value.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Get LV Class Default Value.vi"/>
						<Item Name="Get PString__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get PString__ogtk.vi"/>
						<Item Name="List Directory and LLBs.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/List Directory and LLBs.vi"/>
						<Item Name="MGI String Pattern Match Exists.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_MGI/String/MGI String Pattern Match Exists.vi"/>
						<Item Name="NI_LVConfig.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/config.llb/NI_LVConfig.lvlib"/>
						<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
						<Item Name="Recursive File List.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Recursive File List.vi"/>
						<Item Name="Sort 1D Array (String)__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Sort 1D Array (String)__ogtk.vi"/>
						<Item Name="Split Cluster TD__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Split Cluster TD__ogtk.vi"/>
						<Item Name="subTimeDelay.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
						<Item Name="Trim Whitespace.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
						<Item Name="Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Enumeration__ogtk.ctl"/>
						<Item Name="Type Descriptor Header__ogtk.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Header__ogtk.ctl"/>
						<Item Name="Type Descriptor__ogtk.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor__ogtk.ctl"/>
						<Item Name="USB-I2C Create.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/USB-I2C Create/USB-I2C Create.lvclass"/>
						<Item Name="USB-I2C.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/USB-I2C/USB-I2C.lvclass"/>
						<Item Name="VI Tree.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/VI Tree.vi"/>
						<Item Name="whitespace.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
					</Item>
				</Item>
			</Item>
		</Item>
		<Item Name="Tester" Type="Folder">
			<Item Name="VI Tree.vi" Type="VI" URL="../QSFP28/VI Tree.vi"/>
			<Item Name="Test QSFP28 API.vi" Type="VI" URL="../QSFP28/Test QSFP28 API.vi"/>
		</Item>
		<Item Name="Dll" Type="Folder">
			<Item Name="lvinput.dll" Type="Document" URL="/&lt;resource&gt;/lvinput.dll"/>
		</Item>
		<Item Name="Support Library" Type="Folder">
			<Item Name="Type Sensitive Popup.lvlib" Type="Library" URL="../../../Libraries - Code/Type Sensitive Popup/Type Sensitive Popup.lvlib"/>
		</Item>
		<Item Name="QSFP28.lvlib" Type="Library" URL="../QSFP28/QSFP28.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="LVPositionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPositionTypeDef.ctl"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="Color to RGB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/Color to RGB.vi"/>
				<Item Name="RGB to Color.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/RGB to Color.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="usereventprio.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/usereventprio.ctl"/>
				<Item Name="Rendezvous RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous RefNum"/>
				<Item Name="Create Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Create Rendezvous.vi"/>
				<Item Name="Rendezvous Name &amp; Ref DB Action.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous Name &amp; Ref DB Action.ctl"/>
				<Item Name="Rendezvous Name &amp; Ref DB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous Name &amp; Ref DB.vi"/>
				<Item Name="Not A Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Not A Rendezvous.vi"/>
				<Item Name="RendezvousDataCluster.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/RendezvousDataCluster.ctl"/>
				<Item Name="Create New Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Create New Rendezvous.vi"/>
				<Item Name="AddNamedRendezvousPrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/AddNamedRendezvousPrefix.vi"/>
				<Item Name="GetNamedRendezvousPrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/GetNamedRendezvousPrefix.vi"/>
				<Item Name="Wait at Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Wait at Rendezvous.vi"/>
				<Item Name="Release Waiting Procs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Release Waiting Procs.vi"/>
				<Item Name="Delacor_lib_QMH_Cloneable Module Admin.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Delacor/Delacor QMH/Libraries/Cloneable Module Admin_class/Delacor_lib_QMH_Cloneable Module Admin.lvclass"/>
				<Item Name="Delacor_lib_QMH_Module Admin.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Delacor/Delacor QMH/Libraries/Module Admin_class/Delacor_lib_QMH_Module Admin.lvclass"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Delacor_lib_QMH_Message Queue.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Delacor/Delacor QMH/Libraries/Message Queue_class/Delacor_lib_QMH_Message Queue.lvclass"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="Destroy Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Destroy Rendezvous.vi"/>
				<Item Name="Destroy A Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Destroy A Rendezvous.vi"/>
				<Item Name="RemoveNamedRendezvousPrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/RemoveNamedRendezvousPrefix.vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Close File+.vi"/>
				<Item Name="compatReadText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReadText.vi"/>
				<Item Name="Read File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read File+ (string).vi"/>
				<Item Name="Open File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Open File+.vi"/>
				<Item Name="Read Lines From File (with error IO).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Lines From File (with error IO).vi"/>
				<Item Name="Read Delimited Spreadsheet (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (string).vi"/>
				<Item Name="Read Delimited Spreadsheet (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (I64).vi"/>
				<Item Name="Read Delimited Spreadsheet (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (DBL).vi"/>
				<Item Name="Read Delimited Spreadsheet.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet.vi"/>
				<Item Name="Write Spreadsheet String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Spreadsheet String.vi"/>
				<Item Name="Write Delimited Spreadsheet (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (string).vi"/>
				<Item Name="Write Delimited Spreadsheet (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (I64).vi"/>
				<Item Name="Write Delimited Spreadsheet (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (DBL).vi"/>
				<Item Name="Write Delimited Spreadsheet.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet.vi"/>
				<Item Name="subTimeDelay.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
				<Item Name="LVRowAndColumnTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRowAndColumnTypeDef.ctl"/>
				<Item Name="SetTransparency(Polymorphic)__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/SetTransparency(Polymorphic)__lava_lib_ui_tools.vi"/>
				<Item Name="SetTransparency__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/SetTransparency__lava_lib_ui_tools.vi"/>
				<Item Name="SetTransparency(U8)__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/SetTransparency(U8)__lava_lib_ui_tools.vi"/>
				<Item Name="Fade (Polymorphic)__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/Fade (Polymorphic)__lava_lib_ui_tools.vi"/>
				<Item Name="_Fade(I32)__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/_Fade(I32)__lava_lib_ui_tools.vi"/>
				<Item Name="_fadetype__lava_lib_ui_tools.ctl" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/_fadetype__lava_lib_ui_tools.ctl"/>
				<Item Name="_ExponentialCalculation__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/_ExponentialCalculation__lava_lib_ui_tools.vi"/>
				<Item Name="_Fade(String)__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/_Fade(String)__lava_lib_ui_tools.vi"/>
				<Item Name="Fade__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/Fade__lava_lib_ui_tools.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="Has LLB Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Has LLB Extension.vi"/>
				<Item Name="Get VI Library File Info.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get VI Library File Info.vi"/>
				<Item Name="Librarian File Info Out.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian File Info Out.ctl"/>
				<Item Name="Librarian.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian.vi"/>
				<Item Name="Librarian File Info In.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian File Info In.ctl"/>
				<Item Name="Librarian File List.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian File List.ctl"/>
				<Item Name="VISA Find Search Mode.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Find Search Mode.ctl"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="Rectangle Size__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Alignment/Rectangle Size__lava_lib_ui_tools.vi"/>
				<Item Name="RectSize.vi" Type="VI" URL="/&lt;vilib&gt;/picture/PictureSupport.llb/RectSize.vi"/>
				<Item Name="GPMath.lvlib" Type="Library" URL="/&lt;vilib&gt;/GPower/Math/GPMath.lvlib"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="GPTiming.lvlib" Type="Library" URL="/&lt;vilib&gt;/GPower/Timing/GPTiming.lvlib"/>
				<Item Name="LVDateTimeRec.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDateTimeRec.ctl"/>
				<Item Name="CenterRectInBnds.vi" Type="VI" URL="/&lt;vilib&gt;/picture/PictureSupport.llb/CenterRectInBnds.vi"/>
				<Item Name="RectCentroid.vi" Type="VI" URL="/&lt;vilib&gt;/picture/PictureSupport.llb/RectCentroid.vi"/>
				<Item Name="POffsetRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/PictureSupport.llb/POffsetRect.vi"/>
				<Item Name="RectAndRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/PictureSupport.llb/RectAndRect.vi"/>
				<Item Name="compatReturnToEnter.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReturnToEnter.vi"/>
				<Item Name="PointInRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/PictureSupport.llb/PointInRect.vi"/>
				<Item Name="GPString.lvlib" Type="Library" URL="/&lt;vilib&gt;/GPower/String/GPString.lvlib"/>
				<Item Name="GPNumeric.lvlib" Type="Library" URL="/&lt;vilib&gt;/GPower/Numeric/GPNumeric.lvlib"/>
				<Item Name="LabVIEWHTTPClient.lvlib" Type="Library" URL="/&lt;vilib&gt;/httpClient/LabVIEWHTTPClient.lvlib"/>
				<Item Name="Path To Command Line String.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Path To Command Line String.vi"/>
				<Item Name="PathToUNIXPathString.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/CFURL.llb/PathToUNIXPathString.vi"/>
				<Item Name="Open URL in Default Browser.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser.vi"/>
				<Item Name="Open URL in Default Browser (string).vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser (string).vi"/>
				<Item Name="Open URL in Default Browser core.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser core.vi"/>
				<Item Name="Open URL in Default Browser (path).vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser (path).vi"/>
				<Item Name="Path to URL.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Path to URL.vi"/>
				<Item Name="Escape Characters for HTTP.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Escape Characters for HTTP.vi"/>
				<Item Name="MD5Checksum File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/MD5Checksum.llb/MD5Checksum File.vi"/>
				<Item Name="MD5Checksum format message-digest.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/MD5Checksum.llb/MD5Checksum format message-digest.vi"/>
				<Item Name="MD5Checksum core.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/MD5Checksum.llb/MD5Checksum core.vi"/>
				<Item Name="MD5Checksum pad.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/MD5Checksum.llb/MD5Checksum pad.vi"/>
			</Item>
			<Item Name="user.lib" Type="Folder">
				<Item Name="Get Strings from Enum__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Strings from Enum__ogtk.vi"/>
				<Item Name="Variant to Header Info__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Variant to Header Info__ogtk.vi"/>
				<Item Name="Type Descriptor Header__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Header__ogtk.ctl"/>
				<Item Name="Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Enumeration__ogtk.ctl"/>
				<Item Name="Type Descriptor__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor__ogtk.ctl"/>
				<Item Name="Get Header from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Header from TD__ogtk.vi"/>
				<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
				<Item Name="Get Strings from Enum TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Strings from Enum TD__ogtk.vi"/>
				<Item Name="Get PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get PString__ogtk.vi"/>
				<Item Name="Get Last PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Last PString__ogtk.vi"/>
				<Item Name="Get Data Name from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Data Name from TD__ogtk.vi"/>
				<Item Name="Split Cluster TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Split Cluster TD__ogtk.vi"/>
				<Item Name="Get Cluster Elements TDs__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Elements TDs__ogtk.vi"/>
				<Item Name="Get Cluster Element Names__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Element Names__ogtk.vi"/>
				<Item Name="MGI Caller&apos;s VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Caller&apos;s VI Reference.vi"/>
				<Item Name="MGI Level&apos;s VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Level&apos;s VI Reference.vi"/>
				<Item Name="MGI Top Level VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Top Level VI Reference.vi"/>
				<Item Name="MGI Current VI&apos;s Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Current VI&apos;s Reference.vi"/>
				<Item Name="MGI VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference.vi"/>
				<Item Name="MGI Defer Panel Updates.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Defer Panel Updates.vi"/>
				<Item Name="Current VIs Parents Ref__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/appcontrol/appcontrol.llb/Current VIs Parents Ref__ogtk.vi"/>
				<Item Name="Get TDEnum from Data__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get TDEnum from Data__ogtk.vi"/>
				<Item Name="Set Cluster Element by Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Set Cluster Element by Name__ogtk.vi"/>
				<Item Name="Set Data Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Set Data Name__ogtk.vi"/>
				<Item Name="Get Variant Attributes__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Variant Attributes__ogtk.vi"/>
				<Item Name="Get Data Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Data Name__ogtk.vi"/>
				<Item Name="Cluster to Array of VData__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Cluster to Array of VData__ogtk.vi"/>
				<Item Name="Parse String with TDs__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Parse String with TDs__ogtk.vi"/>
				<Item Name="Array of VData to VCluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Array of VData to VCluster__ogtk.vi"/>
				<Item Name="Get Task List.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/Winevent.llb/Get Task List.vi"/>
				<Item Name="WinUtil Master.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/Winevent.llb/WinUtil Master.vi"/>
				<Item Name="Get LVWUtil32.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/SubVIs/Get LVWUtil32.vi"/>
				<Item Name="Extract Window Names.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/Winevent.llb/Extract Window Names.vi"/>
				<Item Name="Quit Application.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/Winevent.llb/Quit Application.vi"/>
				<Item Name="Window Refnum" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/WINUTIL.LLB/Window Refnum"/>
				<Item Name="PostMessage Master.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/WINUTIL.LLB/PostMessage Master.vi"/>
				<Item Name="Get Window RefNum.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/WINUTIL.LLB/Get Window RefNum.vi"/>
				<Item Name="Not a Window Refnum" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/WINUTIL.LLB/Not a Window Refnum"/>
				<Item Name="String to 1D Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/String to 1D Array__ogtk.vi"/>
				<Item Name="Create Dir if Non-Existant__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Create Dir if Non-Existant__ogtk.vi"/>
				<Item Name="Strip Path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Strip Path__ogtk.vi"/>
				<Item Name="Strip Path - Arrays__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Strip Path - Arrays__ogtk.vi"/>
				<Item Name="Strip Path - Traditional__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Strip Path - Traditional__ogtk.vi"/>
				<Item Name="Build Path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path__ogtk.vi"/>
				<Item Name="Build Path - Traditional__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path - Traditional__ogtk.vi"/>
				<Item Name="Build Path - File Names Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path - File Names Array__ogtk.vi"/>
				<Item Name="Build Path - File Names and Paths Arrays__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path - File Names and Paths Arrays__ogtk.vi"/>
				<Item Name="Build Path - Traditional - path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path - Traditional - path__ogtk.vi"/>
				<Item Name="Build Path - File Names Array - path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path - File Names Array - path__ogtk.vi"/>
				<Item Name="Build Path - File Names and Paths Arrays - path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path - File Names and Paths Arrays - path__ogtk.vi"/>
				<Item Name="List Directory Recursive__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/List Directory Recursive__ogtk.vi"/>
				<Item Name="List Directory__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/List Directory__ogtk.vi"/>
				<Item Name="Trim Whitespace__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace__ogtk.vi"/>
				<Item Name="Trim Whitespace (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace (String)__ogtk.vi"/>
				<Item Name="Trim Whitespace (String Array)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace (String Array)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (Path)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (I16)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (I32)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (I8)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (String)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U16)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U32)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U8)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (Variant)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (I64)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U64)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="Filter 1D Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array__ogtk.vi"/>
				<Item Name="Filter 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (String)__ogtk.vi"/>
				<Item Name="Search Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search Array__ogtk.vi"/>
				<Item Name="Search 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Search 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Search 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Search 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Search 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Search 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Search 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (I8)__ogtk.vi"/>
				<Item Name="Search 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (I16)__ogtk.vi"/>
				<Item Name="Search 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (I32)__ogtk.vi"/>
				<Item Name="Search 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (U8)__ogtk.vi"/>
				<Item Name="Search 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (U16)__ogtk.vi"/>
				<Item Name="Search 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (U32)__ogtk.vi"/>
				<Item Name="Search 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="Search 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (String)__ogtk.vi"/>
				<Item Name="Search 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (Path)__ogtk.vi"/>
				<Item Name="Search 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (Variant)__ogtk.vi"/>
				<Item Name="Search 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (U64)__ogtk.vi"/>
				<Item Name="Search 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (I64)__ogtk.vi"/>
				<Item Name="Search 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="Delete Elements from Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from Array__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="Sort Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort Array__ogtk.vi"/>
				<Item Name="Sort 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Reorder 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Sort 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Reorder 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Sort 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Reorder 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Sort 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Sort 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (I16)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (I32)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (I8)__ogtk.vi"/>
				<Item Name="Sort 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (U16)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (U32)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (U8)__ogtk.vi"/>
				<Item Name="Sort 2D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (CDB)__ogtk.vi"/>
				<Item Name="Sort 2D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (CXT)__ogtk.vi"/>
				<Item Name="Sort 2D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (CSG)__ogtk.vi"/>
				<Item Name="Sort 2D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (DBL)__ogtk.vi"/>
				<Item Name="Sort 2D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (EXT)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (I16)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (I32)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (I8)__ogtk.vi"/>
				<Item Name="Sort 2D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (SGL)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (U16)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (U32)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (U8)__ogtk.vi"/>
				<Item Name="Sort 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (Path)__ogtk.vi"/>
				<Item Name="Sort 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (String)__ogtk.vi"/>
				<Item Name="Sort 2D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (Path)__ogtk.vi"/>
				<Item Name="Sort 2D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (String)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (I64)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (U64)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (I64)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (U64)__ogtk.vi"/>
				<Item Name="Reorder Array2__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder Array2__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (CDB)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (CXT)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (CSG)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (DBL)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (EXT)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (I16)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (I32)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (I8)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (SGL)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (U16)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (U32)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (U8)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (Boolean)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (String)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (Path)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (Variant)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (Boolean)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (CDB)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (CSG)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (CXT)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (DBL)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (EXT)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (I8)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (I16)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (I32)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (Path)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (SGL)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (String)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (U8)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (U16)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (U32)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (Variant)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (I64)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (U64)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (I64)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (U64)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (LVObject)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (LVObject)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (I8)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (I16)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (I32)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (Path)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (String)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (U8)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (U16)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (U32)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (Variant)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (Boolean)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (CDB)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (CSG)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (CXT)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (DBL)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (EXT)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (I8)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (I16)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (I32)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (Path)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (SGL)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (String)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (U8)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (U16)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (U32)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (Variant)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (I64)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (U64)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (I64)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (U64)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (LVObject)__ogtk.vi"/>
				<Item Name="Filter 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (U8)__ogtk.vi"/>
				<Item Name="Filter 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (Path)__ogtk.vi"/>
				<Item Name="Filter 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (U32)__ogtk.vi"/>
				<Item Name="Filter 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (U16)__ogtk.vi"/>
				<Item Name="Filter 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (I16)__ogtk.vi"/>
				<Item Name="Filter 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (I32)__ogtk.vi"/>
				<Item Name="Filter 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (I8)__ogtk.vi"/>
				<Item Name="Filter 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (Variant)__ogtk.vi"/>
				<Item Name="Filter 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="Filter 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Filter 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Filter 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Filter 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Filter 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Filter 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (Boolean)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (CDB)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (CSG)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (CXT)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (DBL)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (EXT)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (I8)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (I16)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (I32)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (Path)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (SGL)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (String)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (U8)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (U16)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (U32)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (Variant)__ogtk.vi"/>
				<Item Name="Filter 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (I64)__ogtk.vi"/>
				<Item Name="Filter 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (U64)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (I64)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (U64)__ogtk.vi"/>
				<Item Name="Filter 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (LVObject)__ogtk.vi"/>
				<Item Name="MGI Get VI Control Ref[].vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Get VI Control Ref[].vi"/>
				<Item Name="ClassID Names Enum__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/appcontrol/appcontrol.llb/ClassID Names Enum__ogtk.ctl"/>
				<Item Name="AES Algorithm.vi" Type="VI" URL="/&lt;userlib&gt;/Crypto-Tools/Crypto.llb/AES Algorithm.vi"/>
				<Item Name="AES Keygenerator.vi" Type="VI" URL="/&lt;userlib&gt;/Crypto-Tools/Crypto.llb/AES Keygenerator.vi"/>
				<Item Name="AES poly mult.vi" Type="VI" URL="/&lt;userlib&gt;/Crypto-Tools/Crypto.llb/AES poly mult.vi"/>
				<Item Name="AES Rounds .vi" Type="VI" URL="/&lt;userlib&gt;/Crypto-Tools/Crypto.llb/AES Rounds .vi"/>
				<Item Name="Get Cluster Element by Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Element by Name__ogtk.vi"/>
				<Item Name="Show Window.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/WINUTIL.LLB/Show Window.vi"/>
				<Item Name="Maximize Window.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/WINUTIL.LLB/Maximize Window.vi"/>
				<Item Name="Make Window Always on Top.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/WINUTIL.LLB/Make Window Always on Top.vi"/>
				<Item Name="Set Window Z-Position.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/WINUTIL.LLB/Set Window Z-Position.vi"/>
				<Item Name="Slice String 1__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Slice String 1__ogtk.vi"/>
				<Item Name="1D Array to String__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/1D Array to String__ogtk.vi"/>
				<Item Name="End of Line Constant (bug fix).vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/End of Line Constant (bug fix).vi"/>
			</Item>
			<Item Name="user32.dll" Type="Document" URL="user32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="VI.lvlib" Type="Library" URL="/&lt;menus&gt;/Categories/User Library/Application Control/VI Refnum/VI.lvlib"/>
			<Item Name="Control Refnum.lvlib" Type="Library" URL="/&lt;menus&gt;/Categories/User Library/Application Control/Control Refnum/Control Refnum.lvlib"/>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Luxshare-OET API.lvlib" Type="Library" URL="/&lt;menus&gt;/Categories/Luxshare-OET/Luxshare-OET API.lvlib"/>
			<Item Name="System Exec.lvlib" Type="Library" URL="/&lt;menus&gt;/Categories/System Exec/System Exec.lvlib"/>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Authorization.lvlib" Type="Library" URL="/&lt;menus&gt;/Categories/Luxshare-OET/Authorization/Authorization.lvlib"/>
			<Item Name="Get BIOS Info.vi" Type="VI" URL="/&lt;menus&gt;/Categories/Luxshare-OET/Authorization/SubVIs/Get BIOS Info.vi"/>
			<Item Name="System.Management" Type="Document" URL="System.Management">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="mscorlib" Type="VI" URL="mscorlib">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Get Volume Info.vi" Type="VI" URL="/&lt;menus&gt;/Categories/Luxshare-OET/Authorization/SubVIs/Get Volume Info.vi"/>
			<Item Name="Mode-Enum.ctl" Type="VI" URL="/&lt;menus&gt;/Categories/Luxshare-OET/Authorization/Control/Mode-Enum.ctl"/>
			<Item Name="Get Key.vi" Type="VI" URL="/&lt;menus&gt;/Categories/Luxshare-OET/Authorization/SubVIs/Get Key.vi"/>
			<Item Name="Encrypt &amp; Decrypt String.vi" Type="VI" URL="/&lt;menus&gt;/Categories/Luxshare-OET/Authorization/SubVIs/Encrypt &amp; Decrypt String.vi"/>
			<Item Name="Cloud Storage.lvlib" Type="Library" URL="/&lt;menus&gt;/Categories/Luxshare-OET/Cloud Storage/Cloud Storage.lvlib"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="QSFP28" Type="Packed Library">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{E7275B6F-A0BA-4477-AC69-488D38F10A14}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">V2.2.7-20221103
1.Page00 Fix Byte192、193 and 220 Write bug, value will changed by control value

V2.2.6-20211021
1.Fix Lot Number Write Bug

V2.2.5-20211018
1.Page Remove Info
2.Page delay 100ms=&gt;200ms

V2.2.4-20200827
1.Dropbox Download modify to support search folder of folder
2.File List modify to support search folder of folder

V2.2.3-20200629
1.Dropbox Release Key

V2.2.2-20200325
1.Add Reset Register Names for Page90 to reset table again

V2.2.1-20200217
1.Add Auto Do Read when Select Connections on ID Info Tab

V2.2.0-20200213
1.Add Write Lot Number and WID Numbers by Lot Number Authorization.ini

V2.1.4-20200204
1.Add Select 0 after Read Write Page, Page03 , Page90 and PageB0
2.Add Delay 100ms after Page Write
3.Revision Compliance Control Update
4.Identifier Control Update

V2.1.3-20191105
1.Data_Not_Ready True color=&gt;Red
2.IntL False color=&gt;Red

V2.1.2-20191012
1.PPL Link Change
2.Do validation by DQMH tool
3.Select Connection change to Select Connection By String

V2.1.1-20190314
1.Threshold controls property modifys

V2.1.0-20190305
1.Memory Map Page00 Add Configure to select read whole page or single byte &amp; delay after read for setting

V2.0.8-20180924
1.Table text size 15 to 18

2.0.7-20180804
1.Error Step add Front Panel Update step

2.0.6-20180720
1.Control Function Add Write Target Functions with Tx Disable &amp; TRx CDR

V2.0.5-20180713
1.Initialize Child Object Modify, Put real class for performance
2.Fix slow bug when start module move &lt;Defer Panel Update&gt; Step

V2.0.4-20180629
1.Add Get Conections when error happen

V2.0.3-20180622
1.Open bug fix

V2.0.2-20180530
1.Remove USB-I2C Factory

V2.0.1-20180516
1.USB-I2C Factory Link fix

V2.0.0-20180427
1.Device chanage to USB-I2C

V1.9.5-20180411
1.Fix threshold range bug

V1.9.4-20180312
1.Setting add Sales PC check

V1.9.3-20180129
1.Byte and BIT Changed VI modify

V1.9.2-20180118
1.EEPROM Files rename, Update Start Path modify

V1.9.1-20180102
1.Add API CDR Control

V1.9.0-20171225
1.Add API Power Control

V1.8.9-20171220
1.Get Exe Folder.vi modify to support other project
2.Is Sales PC modify not support to another application

V1.8.8-20171101
1.Folder Change to Modules
2.Update Control Change Load From SubVIss

V1.8.7-20171020
1.TRx Power Add Unit Select
2.Get Monitors TRx Power add Default unit to dBm
3.Voltage Display Modify Precision to 4

V1.8.6-20171016
1.Fix bug after update file if file empty
2.Modify sales pc function read from pc name config

V1.8.5-20171013
1.Add Update Start Path when doing file,it will auto select file and folder

V1.8.4-20170930
1.Disable Write Control when pc name is on sales list

V1.8.3-20170925
1.Table Set Function Changed

V1.8.2-20170912
1.Add Read Upper Page API

V1.8.1-20170911
1.Fix can't write threshold value, because add nopassword make this happen

V1.8.0-20170904
1.Slope Multiply and Divide APIs Add

V1.7.5-20170824
1.Page3 Write Page and Table Page3 功能修改為234-241Byte無密碼保護

V1.7.4-20170818
1.Revision Compliance 新增 Rev 2.8

V1.7.3-20170817
1.ID Info Page add Right Click for get data to clipboard

V1.7.2-20170811
1.Update Device Object Modify

V1.7.1-20170809
1.File Description Modify

V1.7.0-20170731
1.API Function Add

V1.6.0-20170723
1.新增通訊異常畫面顯示

V1.5.6-20170721
1.Length unit 2 change to 1

V1.5.5-20170718
1.修正Defer Panel Update Caller VI Refnum inalild Bug
2.Update Device Object Add Set Slave Address
3.inititlize step cancel "Get Connections and after set subpanel add this

V1.5.4-20170714
1.Update Device Object API新增Password Step

V1.5.3-20170713
1.修正Internal Page執行寫入後，201以後的BYTE被寫入FF

V1.5.2-20170711
1.修正Auto Read後若回到選單，Device未執行關閉，必須插拔才可使用之情形

V1.5.1
1.修正寫入A0A2時，Vendor Specific值被覆蓋情形

V1.5.0-20170627
1.新增Lower Page、Upper Page、Upper Page3、Internal Calibration Table Write功能
2.變更Create Files方式，取值改由TABLE


V1.4.0-20170621
1.Memory Map顯示方式更改為TABLE

V1.3.0.0
1.Fibre Channel Speed 單數選項更改為複數，Ring change to cluster

V1.2.0.0 
Spec comp codes add No Defind Select

V1.1.0.0
Remove time dealy after close function in case "Close"

Under 1.1.0.0 is beta version</Property>
				<Property Name="Bld_buildSpecName" Type="Str">QSFP28</Property>
				<Property Name="Bld_excludeDependentPPLs" Type="Bool">true</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/E/Luxshare-OET/Exe/Luxshare-OET/Modules</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{BE44EE7B-FD4F-42D4-AB2C-95DA7B4DB359}</Property>
				<Property Name="Bld_version.build" Type="Int">105</Property>
				<Property Name="Bld_version.major" Type="Int">2</Property>
				<Property Name="Bld_version.minor" Type="Int">2</Property>
				<Property Name="Bld_version.patch" Type="Int">7</Property>
				<Property Name="Destination[0].destName" Type="Str">QSFP28.lvlibp</Property>
				<Property Name="Destination[0].path" Type="Path">/E/Luxshare-OET/Exe/Luxshare-OET/Modules/NI_AB_PROJECTNAME.lvlibp</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/E/Luxshare-OET/Exe/Luxshare-OET/Modules</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[2].destName" Type="Str">Config</Property>
				<Property Name="Destination[2].path" Type="Path">/E/Luxshare-OET/Exe/Luxshare-OET/Config</Property>
				<Property Name="Destination[2].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[3].destName" Type="Str">Dll</Property>
				<Property Name="Destination[3].path" Type="Path">/E/Luxshare-OET/Exe/Luxshare-OET/Dll</Property>
				<Property Name="Destination[3].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">4</Property>
				<Property Name="PackedLib_callersAdapt" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{1064354D-76EA-4E66-9B8C-4CCB428E46F5}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[1].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[1].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">3</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Dll</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[1].type" Type="Str">Container</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/QSFP28.lvlib</Property>
				<Property Name="Source[2].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[2].Library.atomicCopy" Type="Bool">true</Property>
				<Property Name="Source[2].Library.LVLIBPtopLevel" Type="Bool">true</Property>
				<Property Name="Source[2].preventRename" Type="Bool">true</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[2].type" Type="Str">Library</Property>
				<Property Name="Source[3].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[3].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Support Library</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].type" Type="Str">Container</Property>
				<Property Name="SourceCount" Type="Int">4</Property>
				<Property Name="TgtF_companyName" Type="Str">Luxshare-Tech</Property>
				<Property Name="TgtF_fileDescription" Type="Str">MSA 8636 for QSFP10&amp;28</Property>
				<Property Name="TgtF_internalName" Type="Str">QSFP28</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright (c) 2017-2022 Luxshare-Tech Corporation. All rights reserved</Property>
				<Property Name="TgtF_productName" Type="Str">QSFP28</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{B5A57D7F-51DF-435E-8D69-71943585C4F7}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">QSFP28.lvlibp</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
		</Item>
	</Item>
</Project>
